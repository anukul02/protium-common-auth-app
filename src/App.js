import Header from './components/header/header';
import AccountBox from './components/accountBox/accountBox';
import Footer from './components/footer/footer';
import BackgroundImage from './assets/login-bg.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <Header />
      <div className="main-content">
        <div className="account-container">
          <AccountBox />
        </div>
        <img
          src={BackgroundImage}
          alt="protium-bg"
          className="background-image"
        />
      </div>
      <Footer />
    </div>
  );
}

export default App;
