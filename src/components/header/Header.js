import React from 'react';
import HeaderLogo from '../../assets/login-logo.png';
import './style.css';

const Header = () => {
  return (
    <div className="header-container">
      <img src={HeaderLogo} alt="protium-logo" className="header-logo" />
    </div>
  );
};

export default Header;
