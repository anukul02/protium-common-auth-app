import React from 'react';
import './style.css';

const Footer = () => {
  return (
    <footer className="footer-container">
      &copy; 2021 Growth Source Financial Technologies Private Limited. All
      Rights Reserved.
    </footer>
  );
};

export default Footer;
