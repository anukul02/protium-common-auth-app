import React from 'react';
import BasicTabs from '../basicTabs/basicTabs';
import './style.css';

const AccountBox = () => {
  return (
    <div className="box-container">
      <div className="top-container">
        <BasicTabs />
      </div>
    </div>
  );
};

export default AccountBox;
