import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import './register.css';

const Register = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);

  const handleName = (e) => {
    setName(e.target.value);
  };

  const handleEmail = (e) => {
    setEmail(e.target.value);
  };

  const handlePassword = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = (e) => {
    if (
      (email === null || email === '') &&
      (password === null || password === '') &&
      (name === null || name === '')
    ) {
      setError(true);
      setErrorMessage(true);
    } else {
      console.log(
        `You have registered using the name ${name} and email ${email} and password ${password}`
      );
    }
  };

  return (
    <div className="form-container">
      <TextField
        id="outlined-name-1"
        label="Name"
        value={name}
        onChange={handleName}
        className="text-field"
        error={error}
        helperText={errorMessage ? 'Please enter a valid name' : ''}
      />

      <TextField
        id="outlined-name-2"
        label="Email"
        value={email}
        onChange={handleEmail}
        className="text-field"
        error={error}
        helperText={errorMessage ? 'Please enter a valid email' : ''}
      />

      <TextField
        id="outlined-name-3"
        label="Password"
        type="password"
        value={password}
        onChange={handlePassword}
        className="text-field"
        error={error}
        helperText={errorMessage ? 'Please enter a valid phone number' : ''}
      />

      <Button variant="contained" className="submit-btn" onClick={handleSubmit}>
        Register
      </Button>
    </div>
  );
};

export default Register;
