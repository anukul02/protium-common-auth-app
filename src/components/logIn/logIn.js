import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import './login.css';

const LogIn = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);

  const handleEmail = (e) => {
    setEmail(e.target.value);
  };

  const handlePassword = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = (e) => {
    if (
      (email === null || email === '') &&
      (password === null || password === '')
    ) {
      setError(true);
      setErrorMessage(true);
    } else {
      console.log(
        `You have logged in using the email ${email} and password ${password}`
      );
    }
  };

  return (
    <div className="form-container">
      <TextField
        id="outlined-name"
        label="Email"
        value={email}
        onChange={handleEmail}
        className="text-field-login"
        error={error}
        helperText={errorMessage ? 'Please enter a valid email' : ''}
      />

      <TextField
        id="outlined"
        label="Password"
        type="password"
        value={password}
        onChange={handlePassword}
        className="text-field-login"
        error={error}
        helperText={errorMessage ? 'Please enter a valid phone number' : ''}
      />

      <Button
        variant="contained"
        className="submit-btn-register"
        onClick={handleSubmit}
      >
        Log In
      </Button>
    </div>
  );
};

export default LogIn;
